FROM cern/alma9-base as build

RUN dnf install -y git
RUN git clone -b jan24p1 https://github.com/FairRootGroup/FairSoft
WORKDIR /FairSoft
RUN bash legacy/setup-almalinux.sh
RUN mkdir build
WORKDIR /FairSoft/build
RUN cmake -S .. -B . -C ../FairSoftConfig.cmake
RUN cmake --build . -j$(nproc)
WORKDIR /
RUN rm -rf /FairSoft/build

ARG SIMPATH=/FairSoft/install
RUN git clone -b dev https://github.com/FairRootGroup/FairRoot.git
WORKDIR /FairRoot
RUN mkdir build
WORKDIR /FairRoot/build
RUN cmake -DCMAKE_INSTALL_PREFIX="/FairRoot/install" ..
RUN make -j$(nproc) && make install
WORKDIR /
RUN rm -rf /FairRoot/{build,.git}
